package eardic.games.startrek;

import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.graphics.Typeface;
import eardic.games.startrek.implementation.STGame;
import eardic.util.games.framework.Game;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Image;
import eardic.util.games.framework.Music;
import eardic.util.games.framework.Sound;

public class Resources
{
	private static boolean resourcesLoaded = false;

	public static Typeface st_font;

	public static Image menu, akira, raptor, cruiser, cube, queen, sphere,
			romulan, dreadnought, loading_text, gover_text, tap_text;

	public static Image sg, opt, ab, ex, mm, cont, res;

	public static Image hull_bar, shield_bar;

	public static Music theme, bg_theme, phaser, borg_phaser;

	public static Sound explode, torpedo, turret, big_explosion;

	public static Image meteor, planet, torpedo_b, torpedo_y, torpedo_r,
			torpedo_g, turret_r, turret_b, turret_y;

	public static void LoadResources(Game game)
	{
		try
		{
			Graphics g = game.getGraphics();

			// Indicator res
			st_font = Typeface.createFromAsset(((STGame) game).getAssets(),
					"Fonts/star_trek_movie.ttf");

			hull_bar = g.newMaskedImage("Indicators/hull_bar.png",
					Config.ARGB_8888, Color.BLACK);
			shield_bar = g.newMaskedImage("Indicators/shield_bar.png",
					Config.ARGB_8888, Color.BLACK);

			meteor = g.newMaskedImage("Space/meteor.png", Config.ARGB_8888,
					Color.BLACK);
			planet = g.newMaskedImage("Space/planet.png", Config.ARGB_8888,
					Color.BLACK);
			torpedo_r = g.newMaskedImage("Weapons/torpedo_r.png",
					Config.ARGB_8888, Color.BLACK);
			torpedo_b = g.newMaskedImage("Weapons/torpedo_b.png",
					Config.ARGB_8888, Color.BLACK);
			torpedo_y = g.newMaskedImage("Weapons/torpedo_y.png",
					Config.ARGB_8888, Color.BLACK);
			torpedo_g = g.newMaskedImage("Weapons/torpedo_g.png",
					Config.ARGB_8888, Color.BLACK);

			turret_r = g.newMaskedImage("Weapons/turret_r.png",
					Config.ARGB_8888, Color.BLACK);
			turret_b = g.newMaskedImage("Weapons/turret_b.png",
					Config.ARGB_8888, Color.BLACK);
			turret_y = g.newMaskedImage("Weapons/turret_y.png",
					Config.ARGB_8888, Color.BLACK);

			// Loads image resources from assets folder
			menu = g.newMaskedImage("Menu/menu_1.png", Config.ARGB_8888,
					Color.BLACK);

			// Config.ARGB_8888, Color.BLACK is used for masking black
			// background
			sg = g.newMaskedImage("Menu/sg.png", Config.ARGB_8888, Color.BLACK);
			opt = g.newMaskedImage("Menu/opt.png", Config.ARGB_8888,
					Color.BLACK);
			ab = g.newMaskedImage("Menu/ab.png", Config.ARGB_8888, Color.BLACK);
			ex = g.newMaskedImage("Menu/ex.png", Config.ARGB_8888, Color.BLACK);
			mm = g.newMaskedImage("Menu/main_menu.png", Config.ARGB_8888,
					Color.BLACK);
			res = g.newMaskedImage("Menu/restart.png", Config.ARGB_8888,
					Color.BLACK);
			cont = g.newMaskedImage("Menu/continue.png", Config.ARGB_8888,
					Color.BLACK);
			loading_text = g.newMaskedImage("Menu/loadingText.png",
					Config.ARGB_8888, Color.BLACK);
			gover_text = g.newMaskedImage("Menu/gameOver.png",
					Config.ARGB_8888, Color.BLACK);
			tap_text = g.newMaskedImage("Menu/tapToStart.png",
					Config.ARGB_8888, Color.BLACK);

			// Ship resources
			akira = g.newMaskedImage("Ships/akira.png", Config.ARGB_8888,
					Color.BLACK);
			raptor = g.newMaskedImage("Ships/raptor.png", Config.ARGB_8888,
					Color.BLACK);
			cruiser = g.newMaskedImage("Ships/cruiser.png", Config.ARGB_8888,
					Color.BLACK);
			cube = g.newMaskedImage("Ships/cube.png", Config.ARGB_8888,
					Color.BLACK);
			sphere = g.newMaskedImage("Ships/sphere.png", Config.ARGB_8888,
					Color.BLACK);
			queen = g.newMaskedImage("Ships/queen.png", Config.ARGB_8888,
					Color.BLACK);
			dreadnought = g.newMaskedImage("Ships/dreadnought.png",
					Config.ARGB_8888, Color.BLACK);
			romulan = g.newMaskedImage("Ships/romulan.png", Config.ARGB_8888,
					Color.BLACK);

			// Sound res
			theme = game.getAudio().createMusic("Musics/main_theme.ogg");
			phaser = game.getAudio().createMusic("Musics/phaser.ogg");
			borg_phaser = game.getAudio().createMusic("Musics/borg_phaser.ogg");
			torpedo = game.getAudio().createSound("Musics/torpedo.ogg");
			explode = game.getAudio().createSound("Musics/explode.ogg");
			big_explosion = game.getAudio().createSound(
					"Musics/large_explosion.ogg");
			bg_theme = game.getAudio().createMusic("Musics/bg_theme.ogg");
			turret = game.getAudio().createSound("Musics/turret.ogg");

			setResourcesLoaded(true);
		}
		catch (Exception e)
		{
			setResourcesLoaded(false);
			System.err.println("Failed to load resources :" + e.getMessage());
		}
	}

	public static void ReleaseResources()
	{
		try
		{
			if (resourcesLoaded)
			{
				// Image res
				st_font=null;
				hull_bar.dispose();
				shield_bar.dispose();
				meteor.dispose();
				planet.dispose();
				torpedo_r.dispose();
				torpedo_b.dispose();
				torpedo_y.dispose();
				turret_r.dispose();
				turret_b.dispose();
				turret_y.dispose();

				// Menu res
				menu.dispose();
				sg.dispose();
				opt.dispose();
				ab.dispose();
				ex.dispose();
				mm.dispose();
				res.dispose();
				cont.dispose();
				loading_text.dispose();
				gover_text.dispose();
				tap_text.dispose();

				// Ship resources
				akira.dispose();
				raptor.dispose();
				cruiser.dispose();
				cube.dispose();
				sphere.dispose();
				queen.dispose();
				dreadnought.dispose();
				romulan.dispose();

				// Sound res
				theme.dispose();
				phaser.dispose();
				borg_phaser.dispose();
				torpedo.dispose();
				explode.dispose();
				big_explosion.dispose();
				bg_theme.dispose();
				turret.dispose();
			}
		}
		catch (Exception e)
		{
			System.err.println("Failed to release res :" + e.getMessage());
		}
	}

	public static boolean isResourcesLoaded()
	{
		return resourcesLoaded;
	}

	private static void setResourcesLoaded(boolean resourcesLoaded)
	{
		Resources.resourcesLoaded = resourcesLoaded;
	}

}
