package eardic.games.startrek.players;

import eardic.games.startrek.ships.Ship;

public class User extends Player
{
	private int score;

	public User(Ship ship, int life)
	{
		this(ship, life, 0);
	}

	public User(Ship ship, int life, int score)
	{
		super(ship,life);
		this.score=score;
	}

	public int getScore()
	{
		return score;
	}
	
	public void resetScore()
	{
		this.score=0;
	}

	public void addScore(int score)
	{
		this.score += score;
	}
}
