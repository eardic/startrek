package eardic.games.startrek.players;

import java.util.Random;

import eardic.games.startrek.ships.Ship;
import eardic.util.games.framework.Direction;
import eardic.util.games.framework.Graphics;

public class Enemy extends Player
{
	private int value;
	private Graphics g;
	private Direction moveDir = Direction.LEFT;
	private boolean firstRightMove=false;
	private Random rand=new Random();
	
	public Enemy(Ship s, Graphics g, int value)
	{
		super(s, 1);
		this.g = g;
		this.setValue(value);
	}
	
	public int getValue()
	{
		return value;
	}

	public void setValue(int value)
	{
		this.value = value;
	}

	public void update()
	{
		if (!getShip().isExploded())
		{			
			Ship s = getShip();
				
			if(s.getY() <= (int)(g.getHeight() * 0.1))
			{
				moveDir = Direction.DOWN;
			}
			else if(!firstRightMove)
			{
				moveDir = Direction.RIGHT;
				firstRightMove=true;
			}
			else if (s.getX() + s.getWidth() + 20 >= g.getWidth())
			{// If ship closes right then go left
				moveDir = Direction.LEFT;
			}
			else if (s.getX() - 20 <= 0)
			{
				moveDir = Direction.RIGHT;
			}

			switch (moveDir)
			{
				case LEFT:
					s.setX(s.getX() - s.getSpeed());
					break;
				case RIGHT:
					s.setX(s.getX() + s.getSpeed());
					break;
				case DOWN:
					s.setY(s.getY() + s.getSpeed());
					break;
				default:
					break;
			}

			s.update();
			s.fire(rand.nextInt(g.getWidth()), g.getHeight());
		}
	}

	public Graphics getG()
	{
		return g;
	}

	public void setG(Graphics g)
	{
		this.g = g;
	}

}
