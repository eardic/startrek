package eardic.games.startrek.players;

import eardic.games.startrek.ships.Ship;
import eardic.util.games.framework.Graphics;

public abstract class Player
{
	private Ship ship;
	private int life;

	public Player(Ship s, int life)
	{
		this.ship = s;
		this.life = life;
	}
	
	public void hit(int dmg)
	{
		ship.hit(dmg);
		if(ship.isExploded() && life > 0)
		{
			life -= 1;
			ship.setShield(ship.MAX_SHIELD);
			ship.setHull(ship.MAX_HULL);
			ship.setExploded(false);
		}
	}

	public void paint(Graphics g)
	{
		if (!ship.isExploded())
		{
			ship.paint(g);
		}
	}

	public void update()
	{
		if (!ship.isExploded())
		{
			ship.update();
			ship.fire();			
		}
	}

	public Ship getShip()
	{
		return ship;
	}

	public int getX()
	{
		return ship.getX();
	}

	public int getY()
	{
		return ship.getY();
	}

	public void setX(int x)
	{
		ship.setX(x);
	}

	public void setY(int y)
	{
		ship.setY(y);
	}

	public int getLife()
	{
		return life;
	}

	public void setLife(int life)
	{
		if (life >= 0)
			this.life = life;
	}
}
