package eardic.games.startrek;

import android.graphics.Color;
import android.graphics.Bitmap.Config;
import eardic.games.startrek.implementation.STGame;
import eardic.games.startrek.screens.LoadingScreen;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Image;
import eardic.util.games.framework.Screen;

public class StarTrek extends STGame
{
	@Override
	public Screen getInitScreen()
	{
		return new LoadingScreen(this);
	}
}
