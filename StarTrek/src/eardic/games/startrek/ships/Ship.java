package eardic.games.startrek.ships;

import eardic.games.startrek.weapons.Weapon;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Image;
import eardic.util.games.framework.Sound;

public abstract class Ship
{
	public final int MAX_SHIELD, MAX_HULL;
	private int shield, hull, speed;
	private Image ship;
	private Sound explode;
	private int x, y;
	private boolean exploded;

	public Ship(Image img, Sound exp, int x, int y, int maxShield, int maxHull,
			int speed)
	{
		this.speed = speed;
		ship = img;
		this.explode = exp;
		this.x = x;
		this.y = y;
		this.MAX_SHIELD = maxShield;
		this.MAX_HULL = maxHull;
		this.setShield(maxShield);
		this.setHull(maxHull);
		this.setExploded(false);
	}

	public abstract void paint(Graphics g);

	public abstract void update();

	public abstract void fire();

	public abstract void fire(int x, int y);

	public abstract Weapon[] getWeapons();
	
	public void hit(int dmg)
	{
		if (shield > 0)
		{
			shield -= dmg;
		}
		else if (hull > 0)
		{
			hull -= dmg;
		}
		else
		{
			exploded = true;
			explode.play();
		}
	}

	public int getWidth()
	{
		return ship.getWidth();
	}

	public int getHeight()
	{
		return ship.getHeight();
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public Image getImage()
	{
		return ship;
	}

	public Sound getExplodeSound()
	{
		return explode;
	}

	public int getHull()
	{
		return hull;
	}

	public void setHull(int hull)
	{
		this.hull = hull;
	}

	public int getShield()
	{
		return shield;
	}

	public void setShield(int shield)
	{
		this.shield = shield;
	}

	public int getSpeed()
	{
		return speed;
	}

	public void setSpeed(int speed)
	{
		this.speed = speed;
	}

	public boolean isExploded()
	{
		return exploded;
	}

	public void setExploded(boolean exploded)
	{
		this.exploded = exploded;
	}
}
