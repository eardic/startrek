package eardic.games.startrek.implementation;

import java.util.Vector;

import android.view.MotionEvent;
import android.view.View;

public class SingleTouchHandler implements TouchHandler
{
	boolean isTouched = false;
	boolean down = false, up = false, move = false;
	int touchX = 0;
	int touchY = 0;
	Vector<TouchPoint> touchs;
	
	public SingleTouchHandler(View view)
	{
		view.setOnTouchListener(this);
		touchs=new Vector<TouchPoint>();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		synchronized (this)
		{
			down = false;
			up = false;
			move = false;
			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
					down = true;
					isTouched = true;
					break;
				case MotionEvent.ACTION_MOVE:
					move = true;
					isTouched = true;
					break;
				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_UP:
					up = true;
					isTouched = false;
					break;
			}

			touchX = (int) event.getX();
			touchY = (int) event.getY();
			
			touchs.add(new TouchPoint(touchX,touchY));			
			return true;
		}
	}

	@Override
	public boolean isTouchDown()
	{
		synchronized (this)
		{
			return down;
		}
	}

	@Override
	public int getTouchX()
	{
		synchronized (this)
		{
			return touchX;
		}
	}

	@Override
	public int getTouchY()
	{
		synchronized (this)
		{
			return touchY;
		}
	}

	@Override
	public boolean isTouchUp()
	{
		// TODO Auto-generated method stub
		return up;
	}

	@Override
	public boolean isTouchMove()
	{
		// TODO Auto-generated method stub
		return move;
	}

	@Override
	public boolean isTouched()
	{
		return isTouched;
	}

	@Override
	public synchronized Vector<TouchPoint> getTouchs()
	{
		Vector<TouchPoint> newV = new Vector<TouchPoint>(touchs);
		touchs.clear();
		return newV;
	}
}