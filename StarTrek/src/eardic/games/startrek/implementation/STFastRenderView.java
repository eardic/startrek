package eardic.games.startrek.implementation;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

@SuppressLint("ViewConstructor")
public class STFastRenderView extends SurfaceView implements Runnable
{
	public static final float MAX_TIME_DIFF = 3.15f;
	public static final int MAX_WAIT = 3;
	STGame game;
	Bitmap framebuffer;
	Thread renderThread = null;
	SurfaceHolder holder;
	volatile boolean running = false;

	public STFastRenderView(STGame game, Bitmap framebuffer)
	{
		super(game);
		this.game = game;
		this.framebuffer = framebuffer;
		this.holder = getHolder();
	}

	public void resume()
	{
		running = true;
		renderThread = new Thread(this);
		renderThread.start();
	}

	public void run()
	{
		try
		{
			Rect dstRect = new Rect();
			long startTime = System.nanoTime();
			long wait = MAX_WAIT;
			float deltaTime = 0f;
			while (running)
			{
				if (!holder.getSurface().isValid())
					continue;

				deltaTime = (System.nanoTime() - startTime) / 10000000.000f;
				startTime = System.nanoTime();

				// System.err.println("DELTA:"+deltaTime);

				if (deltaTime > MAX_TIME_DIFF)
				{
					deltaTime = MAX_TIME_DIFF;
				}

				game.getCurrentScreen().paint(deltaTime);
				game.getCurrentScreen().update(deltaTime);

				Canvas canvas = holder.lockCanvas();
				canvas.getClipBounds(dstRect);
				canvas.drawBitmap(framebuffer, null, dstRect, null);
				holder.unlockCanvasAndPost(canvas);

				wait = (long) (MAX_WAIT - deltaTime);
				if (wait > 0)
				{
					Thread.sleep(wait);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void pause()
	{
		running = false;
		while (true)
		{
			try
			{
				renderThread.join();
				break;
			}
			catch (InterruptedException e)
			{
				// retry
			}

		}
	}

}
