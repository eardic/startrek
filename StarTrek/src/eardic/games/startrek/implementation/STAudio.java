package eardic.games.startrek.implementation;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import eardic.util.games.framework.Audio;
import eardic.util.games.framework.Music;
import eardic.util.games.framework.Sound;

public class STAudio implements Audio
{
	AssetManager assets;
	SoundPool soundPool;

	public STAudio(Activity activity)
	{
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		this.assets = activity.getAssets();
		this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
	}

	public Music createMusic(String filename)
	{
		try
		{
			AssetFileDescriptor assetDescriptor = assets.openFd(filename);
			return new STMusic(assetDescriptor);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Couldn't load music '" + filename + "'");
		}
	}

	public Sound createSound(String filename)
	{
		try
		{
			AssetFileDescriptor assetDescriptor = assets.openFd(filename);
			int soundId = soundPool.load(assetDescriptor, 1);
			return new STSound(soundPool, soundId);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Couldn't load sound '" + filename + "'");
		}
	}
}
