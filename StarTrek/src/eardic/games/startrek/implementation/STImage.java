package eardic.games.startrek.implementation;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import eardic.util.games.framework.Image;

public class STImage implements Image
{
	private Bitmap bitmap;
	private Config format;

	public STImage(Bitmap bitmap, Config format)
	{
		this.bitmap = bitmap;
		this.format = format;
	}
	
	public Bitmap getBitmap()
	{
		return this.bitmap;
	}
	
	public void setBitmap(Bitmap map)
	{
		this.bitmap = map;
	}

	@Override
	public int getWidth()
	{
		return bitmap.getWidth();
	}

	@Override
	public int getHeight()
	{
		return bitmap.getHeight();
	}

	@Override
	public Config getFormat()
	{
		return format;
	}

	@Override
	public void dispose()
	{
		bitmap.recycle();
	}

}
