package eardic.games.startrek.implementation;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import eardic.util.games.framework.Music;

public class STMusic implements Music, OnCompletionListener,
		OnSeekCompleteListener, OnPreparedListener, OnVideoSizeChangedListener
{
	private MediaPlayer mediaPlayer;
	private AssetFileDescriptor asset;

	public STMusic(AssetFileDescriptor assetDescriptor)
	{
		this.asset = assetDescriptor;
		init();
	}

	private void init()
	{
		try
		{
			mediaPlayer = new MediaPlayer();

			mediaPlayer.setDataSource(asset.getFileDescriptor(),
					asset.getStartOffset(), asset.getLength());

			mediaPlayer.prepare();
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setOnSeekCompleteListener(this);
			mediaPlayer.setOnPreparedListener(this);
			mediaPlayer.setOnVideoSizeChangedListener(this);
		}
		catch (Exception e)
		{
			throw new RuntimeException("Couldn't load music");
		}
	}

	@Override
	public void dispose()
	{
		if (this.mediaPlayer.isPlaying())
		{
			this.mediaPlayer.stop();
		}
		this.mediaPlayer.release();
	}

	@Override
	public boolean isLooping()
	{
		return mediaPlayer.isLooping();
	}

	@Override
	public boolean isPlaying()
	{
		return this.mediaPlayer.isPlaying();
	}

	@Override
	public void pause()
	{
		if (this.mediaPlayer.isPlaying())
			mediaPlayer.pause();
	}

	@Override
	public void play()
	{
		try
		{
			synchronized (this)
			{
				if (!this.mediaPlayer.isPlaying())
				{
					mediaPlayer.start();
				}
			}
		}
		catch (IllegalStateException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void setLooping(boolean isLooping)
	{
		mediaPlayer.setLooping(isLooping);
	}

	@Override
	public void setVolume(float volume)
	{
		mediaPlayer.setVolume(volume, volume);
	}

	@Override
	public void stop()
	{
		try
		{
			if (this.mediaPlayer.isPlaying())
			{
				this.mediaPlayer.stop();
				mediaPlayer.prepare();
			}
		}
		catch (IllegalStateException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onCompletion(MediaPlayer player)
	{
		/*
		 * try { mediaPlayer.prepare(); } catch (IllegalStateException e) {
		 * e.printStackTrace(); } catch (IOException e) { e.printStackTrace(); }
		 */
	}

	@Override
	public void seekBegin()
	{
		mediaPlayer.seekTo(0);
	}

	@Override
	public void onPrepared(MediaPlayer player)
	{
	}

	@Override
	public void onSeekComplete(MediaPlayer player)
	{
	}

	@Override
	public void onVideoSizeChanged(MediaPlayer player, int width, int height)
	{
	}

	@Override
	public int length()
	{
		int len = mediaPlayer.getDuration();
		if(len < 0)
			return 0;
		else 
			return len;
	}

}
