package eardic.games.startrek.implementation;

import android.media.SoundPool;

import eardic.util.games.framework.Sound;

public class STSound implements Sound
{
	private int soundId;
	private float volume;
	private SoundPool soundPool;

	public STSound(SoundPool soundPool, int soundId)
	{
		setVolume(1f);
		this.soundId = soundId;
		this.soundPool = soundPool;
	}

	@Override
	public void play()
	{
		soundPool.play(soundId, volume, volume, 0, 0, 1);
	}

	@Override
	public void dispose()
	{
		soundPool.unload(soundId);
	}
		
	public void setVolume(float vol)
	{
		if (vol > 0.0)
			this.volume = vol;
	}

}
