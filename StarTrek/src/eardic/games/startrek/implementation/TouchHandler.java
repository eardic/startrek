package eardic.games.startrek.implementation;

import java.util.Vector;

import android.view.View.OnTouchListener;

public interface TouchHandler extends OnTouchListener
{
	public class TouchPoint
	{
		public int x,y;
		public TouchPoint(int x,int y)
		{
			this.x=x;
			this.y=y;
		}
	}
	public boolean isTouched();

	public boolean isTouchDown();

	public boolean isTouchUp();

	public boolean isTouchMove();

	public int getTouchX();

	public int getTouchY();
	
	public Vector<TouchPoint> getTouchs();
}
