package eardic.games.startrek.implementation;

import java.util.Vector;

import eardic.games.startrek.implementation.TouchHandler.TouchPoint;
import eardic.util.games.framework.Input;

import android.content.Context;
import android.view.View;

public class STInput implements Input
{
	TouchHandler touchHandler;

	public STInput(Context context, View view)
	{
		touchHandler = new SingleTouchHandler(view);
	}

	@Override
	public boolean isTouchDown()
	{
		// TODO Auto-generated method stub
		return touchHandler.isTouchDown();
	}

	@Override
	public int getTouchX()
	{
		// TODO Auto-generated method stub
		return touchHandler.getTouchX();
	}

	@Override
	public int getTouchY()
	{
		// TODO Auto-generated method stub
		return touchHandler.getTouchY();
	}
	
	public Vector<TouchPoint> getTouchs()
	{
		// TODO Auto-generated method stub
		return touchHandler.getTouchs();
	}

}
