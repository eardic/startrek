package eardic.games.startrek.implementation;

import java.io.IOException;
import java.io.InputStream;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;

import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Image;

public class STGraphics implements Graphics
{
	AssetManager assets;
	Bitmap frameBuffer;
	Canvas canvas;
	Paint paint;
	Rect srcRect = new Rect();
	Rect dstRect = new Rect();

	public STGraphics(AssetManager assets, Bitmap frameBuffer)
	{
		this.assets = assets;
		this.frameBuffer = frameBuffer;
		this.canvas = new Canvas(frameBuffer);
		this.paint = new Paint();
	}
	
	public Image newImage(String fileName, Config format)
	{
		Options options = new Options();
		options.inPreferredConfig = format;

		InputStream in = null;
		Bitmap bitmap = null;

		try
		{
			in = assets.open(fileName);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			
			if (bitmap == null)
			{
				throw new RuntimeException("Couldn't load bitmap from asset '"
						+ fileName + "'");
			}			
		}
		catch (IOException e)
		{
			throw new RuntimeException("Couldn't load bitmap from asset '"
					+ fileName + "'");
		}
		finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return new STImage(bitmap, format);
	}

	public Image newMaskedImage(String fileName, Config format, int maskColor)
	{
		Options options = new Options();
		options.inPreferredConfig = format;

		InputStream in = null;
		Bitmap bitmap = null;

		try
		{
			in = assets.open(fileName);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			bitmap = bitmap.copy(bitmap.getConfig(),true);
			
			if (bitmap == null)
			{
				throw new RuntimeException("Couldn't load bitmap from asset '"
						+ fileName + "'");
			}
			for (int i = 0; i < bitmap.getWidth(); ++i)
			{
				for (int j = 0; j < bitmap.getHeight(); ++j)
				{
					if (bitmap.getPixel(i, j) == maskColor)
					{
						bitmap.setPixel(i, j, Color.TRANSPARENT);
					}
				}
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException("Couldn't load bitmap from asset '"
					+ fileName + "'");
		}
		finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return new STImage(bitmap, format);
	}

	public void clearScreen(int color)
	{
		canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8,
				(color & 0xff));
	}

	public void drawCircle(int cx, int cy, int radius, int color)
	{
		paint.setColor(color);
		canvas.drawCircle(cx, cy, radius, paint);
	}

	public void drawLine(int x, int y, int x2, int y2, int color)
	{
		paint.setColor(color);
		paint.setAntiAlias(true);
		canvas.drawLine(x, y, x2, y2, paint);
	}

	public void drawRect(int x, int y, int width, int height, int color)
	{
		paint.setColor(color);
		paint.setStyle(Style.FILL);
		canvas.drawRect(x, y, x + width - 1, y + height - 1, paint);
	}

	public void drawARGB(int a, int r, int g, int b)
	{
		paint.setStyle(Style.FILL);
		canvas.drawARGB(a, r, g, b);
	}

	public void drawString(String text, int x, int y, Paint paint)
	{
		canvas.drawText(text, x, y, paint);
	}

	public void drawString(String text, int x, int y, Paint paint, String font)
	{
		Typeface type = Typeface.createFromAsset(assets, font);
		paint.setTypeface(type);
		canvas.drawText(text, x, y, paint);
	}

	public void drawImage(Image Image, int x, int y, int srcX, int srcY,
			int srcWidth, int srcHeight)
	{
		srcRect.left = srcX;
		srcRect.top = srcY;
		srcRect.right = srcX + srcWidth;
		srcRect.bottom = srcY + srcHeight;

		dstRect.left = x;
		dstRect.top = y;
		dstRect.right = x + srcWidth;
		dstRect.bottom = y + srcHeight;

		canvas.drawBitmap(((STImage) Image).getBitmap(), srcRect, dstRect, null);
	}

	public void drawImage(Image image, int x, int y)
	{
		canvas.drawBitmap(((STImage) image).getBitmap(), x, y, null);
	}

	public void drawScaledImage(Image Image, int x, int y, int width,
			int height, int srcX, int srcY, int srcWidth, int srcHeight)
	{

		srcRect.left = srcX;
		srcRect.top = srcY;
		srcRect.right = srcX + srcWidth;
		srcRect.bottom = srcY + srcHeight;

		dstRect.left = x;
		dstRect.top = y;
		dstRect.right = x + width;
		dstRect.bottom = y + height;

		canvas.drawBitmap(((STImage) Image).getBitmap(), srcRect, dstRect, null);

	}

	public int getWidth()
	{
		return frameBuffer.getWidth();
	}

	public int getHeight()
	{
		return frameBuffer.getHeight();
	}

	@Override
	public void drawRotatedImage(Image image, int x, int y, float ang)
	{
		Matrix mx = new Matrix();
		mx.postRotate(ang);
		STImage img = ((STImage) image);
		img.setBitmap(Bitmap.createBitmap(img.getBitmap(), 0, 0,
				img.getWidth(), img.getHeight(), mx, true));
		drawImage(image, x, y);
	}

	@Override
	public Image rotateImage(Image image, float ang)
	{
		Matrix mx = new Matrix();
		mx.postRotate(ang);
		STImage img = ((STImage) image);
		img.setBitmap(Bitmap.createBitmap(img.getBitmap(), 0, 0,
				img.getWidth(), img.getHeight(), mx, true));
		return img;
	}

	@Override
	public void drawPoint(int x, int y, int size, int color)
	{
		paint.setColor(color);
		paint.setStrokeWidth(size);
		canvas.drawPoint(x, y, paint);
	}

}
