package eardic.games.startrek.screens;

import android.graphics.Bitmap.Config;
import android.graphics.Color;
import eardic.games.startrek.Resources;
import eardic.util.games.framework.Game;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Screen;

public class LoadingScreen extends Screen
{
	public LoadingScreen(Game game)
	{
		super(game);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void update(float deltaTime)
	{
		try
		{
			Resources.LoadResources(game);

			// Play bg music
			Resources.theme.setLooping(true);
			Resources.theme.play();

			game.setScreen(new MainMenuScreen(game));
		}
		catch (Exception e)
		{
			System.err.println("Fail updating load screen :" + e.getMessage());
		}

	}

	@Override
	public void paint(float deltaTime)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void backButton()
	{
		// TODO Auto-generated method stub

	}

}
