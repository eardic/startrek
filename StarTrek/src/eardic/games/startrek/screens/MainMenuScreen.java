package eardic.games.startrek.screens;

import android.graphics.Color;
import eardic.games.startrek.Resources;
import eardic.games.startrek.menu.Menu;
import eardic.games.startrek.menu.MenuButton;
import eardic.util.games.framework.Game;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Input;
import eardic.util.games.framework.Screen;

public class MainMenuScreen extends Screen
{
	public static final int BUTTON_MARGIN = 80;
	private Menu menu;

	public MainMenuScreen(Game game)
	{
		super(game);
		init_menu();
	}

	private void init_menu()
	{
		menu = new Menu(BUTTON_MARGIN);
		menu.addButton(new MenuButton(Resources.sg, MenuButton.TYPE.START));
		menu.addButton(new MenuButton(Resources.opt, MenuButton.TYPE.OPTIONS));
		menu.addButton(new MenuButton(Resources.ab, MenuButton.TYPE.ABOUT));
		menu.addButton(new MenuButton(Resources.ex, MenuButton.TYPE.EXIT));
	}

	@Override
	public void update(float deltaTime)
	{
		try
		{
			Input event = game.getInput();

			if (event.isTouchDown())
			{
				for (int i = 0; i < menu.getButtonSize(); ++i)
				{
					MenuButton b = menu.getMenuButton(i);
					if (b.isPressed(event.getTouchX(), event.getTouchY()))
					{
						switch (b.getType())
						{
							case START:
								game.setScreen(new GameScreen(game));
								break;
							case OPTIONS:
								game.setScreen(new OptionsScreen(game));
								break;
							case ABOUT:
								game.setScreen(new AboutScreen(game));
								break;
							case EXIT:
								Resources.ReleaseResources();
								System.exit(0);
								break;
							default:
								break;
						}
					}
				}
			}

			menu.update();
		}
		catch (Exception e)
		{
			System.err.println("Failed to update main menu :" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void paint(float deltaTime)
	{
		try
		{
			Graphics g = game.getGraphics();
			// Draw Menu bg
			g.clearScreen(Color.BLACK);
			g.drawScaledImage(Resources.menu, 0, 0, g.getWidth(),
					g.getHeight(), 0, 0, Resources.menu.getWidth(),
					Resources.menu.getHeight());

			menu.paint(g);
		}
		catch (Exception e)
		{
			System.err.println("Failed to paint main menu :" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void pause()
	{
		Resources.theme.pause();
	}

	@Override
	public void resume()
	{
		Resources.theme.play();
	}

	@Override
	public void dispose()
	{
		menu.clearAll();
		menu = null;
	}

	@Override
	public void backButton()
	{

	}

}
