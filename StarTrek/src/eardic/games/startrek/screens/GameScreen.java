package eardic.games.startrek.screens;

import eardic.games.startrek.Background;
import eardic.games.startrek.CollisionDetection;
import eardic.games.startrek.LevelManager;
import eardic.games.startrek.Resources;
import eardic.games.startrek.implementation.TouchHandler.TouchPoint;
import eardic.games.startrek.indicators.Indicator;
import eardic.games.startrek.indicators.Lifer;
import eardic.games.startrek.indicators.Scorer;
import eardic.games.startrek.menu.Menu;
import eardic.games.startrek.menu.MenuButton;
import eardic.games.startrek.players.Enemy;
import eardic.games.startrek.players.User;
import eardic.games.startrek.ships.Raptor;
import eardic.games.startrek.ships.Ship;
import eardic.util.games.framework.Game;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Image;
import eardic.util.games.framework.Input;
import eardic.util.games.framework.Screen;
import java.util.Vector;

import android.graphics.Color;
import android.graphics.Paint;

public class GameScreen extends Screen
{
	enum GameState
	{
		Ready, Running, Paused, GameOver
	}

	public static final int INIT_LIFE = 5;

	LevelManager lvlMng;
	CollisionDetection collisionDetector;
	GameState state = GameState.Ready;
	User player;
	Background bg;
	Indicator shieldBar, hullBar;
	Scorer scorer;
	Lifer lifer;
	Vector<Enemy> activeEnemies;
	Menu pauseMenu, gameOverMenu;

	public GameScreen(Game game)
	{
		super(game);
		init_lvl();
		init_user();
		init_bg();
		init_indicators();
		init_enemy();
		init_cd();
		init_pause_menu();
		init_over_menu();
	}

	private void init_over_menu()
	{
		gameOverMenu = new Menu();
		gameOverMenu.addButton(new MenuButton(Resources.res,
				MenuButton.TYPE.RESTART));
		gameOverMenu.addButton(new MenuButton(Resources.mm,
				MenuButton.TYPE.MAIN_MENU));
	}

	private void init_pause_menu()
	{
		pauseMenu = new Menu();
		pauseMenu.addButton(new MenuButton(Resources.cont,
				MenuButton.TYPE.CONTINUE));
		pauseMenu.addButton(new MenuButton(Resources.res,
				MenuButton.TYPE.RESTART));
		pauseMenu.addButton(new MenuButton(Resources.mm,
				MenuButton.TYPE.MAIN_MENU));
	}

	private void init_cd()
	{
		collisionDetector = new CollisionDetection(activeEnemies, player);
	}

	private void init_user()
	{
		// Calculate start point of user ship
		int x = game.getGraphics().getWidth() / 2 - Resources.raptor.getWidth()
				/ 2;
		int y = game.getGraphics().getHeight() - Resources.raptor.getHeight();

		// Set default ship to raptor
		Ship ship = new Raptor(Resources.raptor, Resources.phaser,
				Resources.big_explosion, Resources.torpedo, x, y,
				Raptor.MAX_SHIELD, Raptor.MAX_HULL, Raptor.SPEED);

		// Create player
		player = new User(ship, INIT_LIFE);
	}

	private void init_bg()
	{
		Graphics g = game.getGraphics();
		bg = new Background(g.getWidth(), g.getHeight(), player.getShip()
				.getSpeed(), Resources.bg_theme);
	}

	private void init_enemy()
	{
		activeEnemies = new Vector<Enemy>();
		activeEnemies.add(lvlMng.nextEnemy());
	}

	private void init_lvl()
	{
		lvlMng = new LevelManager(game.getGraphics());
	}

	private Indicator init_bar(Image img, int color, int x, int y, int lvl)
	{
		Indicator ind = new Indicator(img, x, y, lvl, lvl);
		ind.setFillX(ind.getX() + 70);
		ind.setFillY(ind.getY() + 35);
		ind.setFillWidth(71);
		ind.setFillHeight(14);
		ind.setFillColor(color);
		return ind;
	}

	private void init_indicators()
	{
		Paint scorePaint = new Paint();
		scorePaint.setTypeface(Resources.st_font);
		scorePaint.setTextSize(20);
		scorePaint.setColor(Color.WHITE);
		scorePaint.setAntiAlias(true);
		Graphics g = game.getGraphics();

		scorer = new Scorer(scorePaint, g.getWidth() / 2 - 60,
				(int) scorePaint.getTextSize() * 2);

		shieldBar = init_bar(Resources.shield_bar, Color.BLUE, 0, 0, player
				.getShip().getShield());
		hullBar = init_bar(Resources.hull_bar, Color.RED, game.getGraphics()
				.getWidth() - Resources.hull_bar.getWidth(), 0, player
				.getShip().getHull());

		lifer = new Lifer(player.getShip().getImage(), g.getWidth()
				- Lifer.LOGO_SIZE, hullBar.getY()
				+ hullBar.getImg().getHeight(), INIT_LIFE);
	}

	@Override
	public void update(float deltaTime)
	{
		Input touch = game.getInput();
		if (state == GameState.Ready)
			updateReady(touch);
		else if (state == GameState.Running)
			updateRunning(touch, deltaTime);
		else if (state == GameState.Paused)
			updatePaused(touch);
		else if (state == GameState.GameOver)
			updateGameOver(touch);
	}

	private void updateReady(Input touch)
	{
		// This example starts with a "Ready" screen.
		// When the user touches the screen, the game begins.
		// state now becomes GameState.Running.
		// Now the updateRunning() method will be called!

		if (touch.getTouchs().size() > 0)
			state = GameState.Running;
	}

	private void updateRunning(Input touch, float deltaTime)
	{
		Vector<TouchPoint> touchs = touch.getTouchs();
		for (int i = 0; i < touchs.size(); ++i)
		{
			TouchPoint p = touchs.get(i);
			int touchX = p.x;
			int touchY = p.y;
			// Set middle of ship to touch point, add 100 for expanding touch
			// area
			if (touchX >= player.getX() - 100
					&& touchX <= player.getX() + player.getShip().getWidth()
							+ 100
					&& touchY >= player.getY() - 100
					&& touchY <= player.getY() + player.getShip().getHeight()
							+ 100)
			{
				player.setX(touchX - player.getShip().getImage().getWidth() / 2);
				player.setY(touchY - player.getShip().getImage().getHeight()
						/ 2);
			}
		}
		// 2. Check miscellaneous events like death:
		if (player.getLife() == 0)
		{
			state = GameState.GameOver;
		}

		// 3. Call individual update() methods here.
		// This is where all the game updates happen.
		// For example, robot.update();

		bg.update();
		player.update();

		int size = activeEnemies.size();
		for (int i = 0; i < size; ++i)
		{
			Enemy e = activeEnemies.get(i);
			e.update();
			if (e.getShip().isExploded())
			{
				player.addScore(e.getValue());
				activeEnemies.remove(e);
				if (lvlMng.enemyLeft() <= 0)
					lvlMng.nextLevel();
				activeEnemies.add(lvlMng.nextEnemy());
			}
		}

		collisionDetector.checkCollision();
		scorer.update(player.getScore());
		lifer.update(player.getLife());
		shieldBar.setLevel(player.getShip().getShield());
		hullBar.setLevel(player.getShip().getHull());
	}

	private void updatePaused(Input touch)
	{
		try
		{
			Input event = game.getInput();
			if (event.isTouchDown())
			{
				for (int i = 0; i < pauseMenu.getButtonSize(); ++i)
				{
					MenuButton b = pauseMenu.getMenuButton(i);
					if (b.isPressed(event.getTouchX(), event.getTouchY()))
					{
						switch (b.getType())
						{
							case CONTINUE:
								state = GameState.Running;
								return;
							case RESTART:
								nullify();
								game.setScreen(new GameScreen(game));
								return;
							case MAIN_MENU:
								nullify();
								game.setScreen(new MainMenuScreen(game));
								return;
							default:
								break;
						}
					}
				}
			}

			pauseMenu.update();

		}
		catch (Exception ex)
		{
			System.err.println("Failed to update paused screen : "
					+ ex.getMessage());
		}
	}

	private void updateGameOver(Input touch)
	{
		try
		{
			Input event = game.getInput();
			if (event.isTouchDown())
			{
				for (int i = 0; i < gameOverMenu.getButtonSize(); ++i)
				{
					MenuButton b = gameOverMenu.getMenuButton(i);
					if (b.isPressed(event.getTouchX(), event.getTouchY()))
					{
						switch (b.getType())
						{
							case RESTART:
								nullify();
								game.setScreen(new GameScreen(game));
								return;
							case MAIN_MENU:
								nullify();
								game.setScreen(new MainMenuScreen(game));
								return;
							default:
								break;
						}
					}
				}
			}

			gameOverMenu.update();

		}
		catch (Exception ex)
		{
			System.err.println("Failed to update game over screen : "
					+ ex.getMessage());
		}
	}

	@Override
	public void paint(float deltaTime)
	{
		// Firstly, background elements
		Graphics g = game.getGraphics();
		g.clearScreen(Color.BLACK);
		bg.paint(g);
		shieldBar.paint(g);
		hullBar.paint(g);
		scorer.paint(g);
		lifer.paint(g);

		// Secondly, draw the UI above the game elements.
		if (state == GameState.Ready)
			drawReadyUI();
		if (state == GameState.Running)
			drawRunningUI();
		if (state == GameState.Paused)
			drawPausedUI();
		if (state == GameState.GameOver)
			drawGameOverUI();

	}

	private void nullify()
	{
		// Set all variables to null. You will be recreating them in the
		// constructor.
		player = null;
		bg = null;
		activeEnemies = null;
		pauseMenu = null;
		lvlMng = null;
		collisionDetector = null;
		shieldBar = null;
		hullBar = null;

		// Call garbage collector to clean up memory.
		System.gc();
	}

	private void drawReadyUI()
	{
		Graphics g = game.getGraphics();
		Image tap = Resources.tap_text;
		g.drawImage(tap, g.getWidth() / 2 - tap.getWidth() / 2, g.getHeight()
				/ 2 - tap.getHeight() / 2);
	}

	private void drawRunningUI()
	{
		// Paint user
		player.paint(game.getGraphics());
		// Paint enemy
		int size = activeEnemies.size();
		for (int i = 0; i < size; ++i)
			activeEnemies.get(i).paint(game.getGraphics());
	}

	private void drawPausedUI()
	{
		Graphics g = game.getGraphics();
		// Darken the entire screen so you can display the Paused screen.
		g.drawARGB(155, 0, 0, 0);
		pauseMenu.paint(g);
	}

	private void drawGameOverUI()
	{
		Graphics g = game.getGraphics();
		Image over = Resources.gover_text;
		g.drawImage(over, g.getWidth() / 2 - over.getWidth() / 2,
				(int) (g.getHeight() * 0.3));
		gameOverMenu.paint(g);
	}

	@Override
	public void pause()
	{
		if (state == GameState.Running)
		{
			state = GameState.Paused;
			bg.getBgMusic().pause();
		}
	}

	@Override
	public void resume()
	{
		bg.getBgMusic().play();
	}

	@Override
	public void dispose()
	{
		nullify();
	}

	@Override
	public void backButton()
	{
		pause();
	}

}
