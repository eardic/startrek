package eardic.games.startrek.screens;

import eardic.games.startrek.Resources;
import eardic.util.games.framework.Game;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Screen;

public class AboutScreen extends Screen
{

	public AboutScreen(Game game)
	{
		super(game);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(float deltaTime)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void paint(float deltaTime)
	{
		Graphics g = game.getGraphics();

		// Draw Menu bg
		g.drawScaledImage(Resources.menu, 0, 0, g.getWidth(), g.getHeight(), 0,
				0, Resources.menu.getWidth(), Resources.menu.getHeight());

	}

	@Override
	public void pause()
	{
		Resources.theme.pause();
	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub
		Resources.theme.play();
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void backButton()
	{
		game.setScreen(new MainMenuScreen(game));
	}

}
