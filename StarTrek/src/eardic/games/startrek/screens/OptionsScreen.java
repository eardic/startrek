package eardic.games.startrek.screens;

import eardic.games.startrek.Resources;
import eardic.util.games.framework.Game;
import eardic.util.games.framework.Graphics;
import eardic.util.games.framework.Screen;

public class OptionsScreen extends Screen
{
	public OptionsScreen(Game game)
	{
		super(game);
	}

	@Override
	public void update(float deltaTime)
	{
		
	}

	@Override
	public void paint(float deltaTime)
	{
		Graphics g = game.getGraphics();

		// Draw Menu bg
		g.drawScaledImage(Resources.menu, 0, 0, g.getWidth(), g.getHeight(), 0,
				0, Resources.menu.getWidth(), Resources.menu.getHeight());		
	}

	@Override
	public void pause()
	{
		Resources.theme.pause();
	}

	@Override
	public void resume()
	{
		Resources.theme.play();
	}

	@Override
	public void dispose()
	{
		
	}

	@Override
	public void backButton()
	{
		game.setScreen(new MainMenuScreen(game));
	}

}
