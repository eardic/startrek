package eardic.util.games.framework;

import android.graphics.Bitmap;

public interface Image {
    public int getWidth();
    public int getHeight();
    public Bitmap.Config getFormat();
    public void dispose();
}
