package eardic.util.games.framework;

import java.util.Vector;

import eardic.games.startrek.implementation.TouchHandler.TouchPoint;

public interface Input
{
	public boolean isTouchDown();
	public int getTouchX();
	public int getTouchY();
	public Vector<TouchPoint> getTouchs();
}
