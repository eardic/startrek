package eardic.util.games.framework;

public interface Sound
{
	public void play();

	public void dispose();
}
