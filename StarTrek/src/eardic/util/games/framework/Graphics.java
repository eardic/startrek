package eardic.util.games.framework;

import android.graphics.Bitmap.Config;
import android.graphics.Paint;

public interface Graphics 
{
    public Image newImage(String fileName, Config format);
    
    public Image newMaskedImage(String fileName, Config format, int maskColor);

    public void clearScreen(int color);

    public void drawLine(int x, int y, int x2, int y2, int color);

    public void drawRect(int x, int y, int width, int height, int color);

    public void drawImage(Image image, int x, int y, int srcX, int srcY,
            int srcWidth, int srcHeight);

    public void drawImage(Image image, int x, int y);
    
    public void drawScaledImage(Image Image, int x, int y, int width,
			int height, int srcX, int srcY, int srcWidth, int srcHeight);

    void drawString(String text, int x, int y, Paint paint);
    
    void drawString(String text, int x, int y, Paint paint,String font);
    
    public int getWidth();

    public int getHeight();
    
    public void drawCircle(int cx,int cy,int radius,int color);

    public void drawARGB(int i, int j, int k, int l);
    
    public void drawRotatedImage(Image image, int x, int y, float ang);
    
    public void drawPoint(int x,int y,int size,int color);
    
    public Image rotateImage(Image image, float ang);
   
}
